# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Banking Import Timeline',
    'name_de_DE': 'Buchhaltung Bankimport Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Timeline for Bank Imports
    - Adds timeline features to the Banking Import Module
''',
    'description_de_DE': '''Gültigkeitsdauer für Bankimport
    - Fügt die Merkmale der Gültigkeitsdauer-Module zu Bankimport hinzu
''',
    'depends': [
        'account_banking_import',
        'account_batch_invoice_timeline',
        'account_batch_timeline',
        ],
    'xml': [
    ],
    'translation': [
    ],
}
