#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch Import Line"
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import Eval, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.batch.import.line'

    def __init__(self):
        super(Line, self).__init__()

        domain_fiscalyear = ('fiscalyear', '=', Eval('fiscalyear'))

        self.account = copy.copy(self.account)
        if self.account.domain is None:
            self.account.domain = [domain_fiscalyear]
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.account.domain):
                self.account.domain += [domain_fiscalyear]
        if 'fiscalyear' not in self.account.depends:
            self.account.depends = copy.copy(self.account.depends)
            self.account.depends.append('fiscalyear')

        self.contra_account = copy.copy(self.contra_account)
        if self.contra_account.domain is None:
            self.contra_account.domain = [domain_fiscalyear]
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.contra_account.domain):
                self.contra_account.domain += [domain_fiscalyear]
        if 'fiscalyear' not in self.contra_account.depends:
            self.contra_account.depends = copy.copy(
                self.contra_account.depends)
            self.contra_account.depends.append('fiscalyear')

        # invoice
        if 'date' not in self.invoice.depends:
            self.invoice.depends = copy.copy(self.invoice.depends)
            self.invoice.depends.append('date')
        if 'invoice' not in self.invoice.on_change:
            self.invoice.on_change = copy.copy(
                self.invoice.on_change)
            self.invoice.on_change.append('invoice')
        if 'date' not in self.invoice.on_change:
            self.invoice.on_change = copy.copy(
                self.invoice.on_change)
            self.invoice.on_change.append('date')

        # contra_account
        if 'invoice' not in self.contra_account.depends:
            self.contra_account.depends = copy.copy(
                self.contra_account.depends)
            self.contra_account.depends.append('invoice')
        if 'invoice' not in self.contra_account.on_change:
            self.contra_account.on_change = copy.copy(
                self.contra_account.on_change)
            self.contra_account.on_change.append('invoice')
        if 'date' not in self.contra_account.on_change:
            self.contra_account.on_change = copy.copy(
                self.contra_account.on_change)
            self.contra_account.on_change.append('date')

        self._reset_columns()

    def default_get(self, fields, with_rec_name=True):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()

        values = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)

        date = values.get('date', today)
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).default_get(fields,
                    with_rec_name=with_rec_name)

        return res

    def on_change_party(self, value):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()
        date = value.get('date', today)
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).on_change_party(value)

        return res

    def on_change_date(self, value):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()
        date = value.get('date', today)
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).on_change_date(value)

        return res

    def _get_invoice_account_id(self, invoice_id, date):
        batch_line_obj = Pool().get('account.batch.line')
        account_id = batch_line_obj._find_account_id_for_date(invoice_id, date)
        return account_id

Line()
