# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL
from trytond.wizard import Wizard
from trytond.transaction import Transaction
from trytond.pool import Pool

_ZERO = Decimal("0.0")

class BankingImportLine(ModelSQL, ModelView):
    _name = 'banking.import.line'

    def _get_account_and_sides(self, batch_id, date, amount):
        with Transaction().set_context(effective_date=date):
            return super(BankingImportLine, self)._get_account_and_sides(
                batch_id, date, amount)

BankingImportLine()


class RunImport(Wizard):
    _name = 'banking.run_import'

    def _get_account_balance(self, account_id, date=None):
        pool = Pool()
        account_obj = pool.get('account.account')
        batch_obj = pool.get('account.batch')

        account_by_date = account_obj.get_account_by_date(account_id, date)
        return batch_obj._calc_start_balance(account_by_date)

RunImport()
